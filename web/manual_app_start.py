#!/usr/bin/env python3
# IMPORT
from eoxer.app import create_app
from eoxer.config import TestConfig
# from eoxer.config import DevConfig
# from eoxer.config import ProdConfig

app = create_app(TestConfig)
''' Entrypoint to run flask manually '''
if __name__ == '__main__':
    # APP RUN
    app.run(
        # host='0.0.0.0',
        port=8080,
        debug=True,
        passthrough_errors=True)
