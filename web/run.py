# IMPORT
from flask.helpers import get_debug_flag

from eoxer.app import create_app
from eoxer.config import DevConfig, ProdConfig

# CONST
CONFIG = DevConfig if get_debug_flag() else ProdConfig

# MAIN
app = create_app(CONFIG)
