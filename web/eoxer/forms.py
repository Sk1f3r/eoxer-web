''' Flask forms to be used on html pages with a set of validators '''
# IMPORT
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms import StringField, validators

# CONST
PID_REGEXP = r'^[A-z\d\s\/\-\.+=]+$'
PID_MSG_LEN = 'Ошибка: не менее 4 символов, не более 200'
PID_MSG_REQ = 'Ошибка: укажите не менее одного артикула'
PID_MSG_REGEXP = 'Ошибка: используйте только латиницу, цифры, пробелы или символы /-.+='

CCFILE_MSG_EXT = 'Ошибка: выберите файл XLS'


# CLASS
class PidForm(FlaskForm):
    ''' Form to be filled by partnumbers '''
    pids = StringField('pids', [
        validators.Length(min=4, max=200, message=PID_MSG_LEN),
        validators.DataRequired(message=PID_MSG_REQ),
        validators.Regexp(PID_REGEXP, message=PID_MSG_REGEXP)
    ])


class CcFileForm(FlaskForm):
    ''' Form to select and upload a new version of Cisco Category XLS File'''
    # ccfile = FileField('ccfile')
    ccfile = FileField(
        'ccfile',
        [FileRequired(),
         FileAllowed(['xls', 'xlsx'], message=CCFILE_MSG_EXT)])
