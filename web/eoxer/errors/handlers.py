from flask import render_template

from eoxer.errors import eh


@eh.app_errorhandler(404)
def error_not_found(error):
    return render_template("errors/404.html"), 404


@eh.app_errorhandler(500)
def error_internal(error):
    return render_template("errors/404.html"), 500
