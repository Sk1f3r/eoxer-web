from flask import Blueprint

eh = Blueprint('errors', __name__)

# the trick to avoid two flake8 error
if eh:
    from eoxer.errors import handlers
    type(handlers)
