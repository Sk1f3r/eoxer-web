# IMPORT
from eoxer.database import DBHelper


# FUNCTION
def search_cc_version(db: str):
    """[Search to a version data]

    Arguments:
        db {str} -- [hostname with a mongo database]

    Returns:
        [str] -- [string with a version like "13.10.2018"]
    """

    client = DBHelper(host=db)
    result = client.find_version()

    if not result:
        return None

    return result['version']


def search_cc_substring(raw_pids: str, db: str):
    """[Search for a substring thru whole database]

    Arguments:
        raw_pids {str} -- [a raw sequence of pids]
        db {str} -- [hostname with a mongo database]

    Returns:
        [tuple] -- [tuple of 5 lists: c1, c2, c3, cX, not_found]
    """

    # preparing data
    pids_list = list({pid.upper().strip() for pid in raw_pids.split()})

    # establishing a session with DB
    client = DBHelper(host=db)
    if not client:
        return None

    # searching
    c1, c2, c3, cX, not_found = [], [], [], [], []
    for pid in pids_list:
        result = client.find_substring(pid)

        if result.count() == 0:
            not_found.append(pid)
            continue

        for match in result:
            item = match['cat_c'].upper()
            if item == "C1":
                c1.append(match)
            elif item == "C2":
                c2.append(match)
            elif item == "C3":
                c3.append(match)
            elif item == "C4" or item == "CL":
                cX.append(match)

    return c1, c2, c3, cX, not_found
