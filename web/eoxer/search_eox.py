# IMPORT
from eoxer.vars import UPDATE_TOKEN_URI, PI_URI, EOX_URI, RESPONSE_TYPE
from eoxer.vars import api_client_creds
from eoxer.vars import log

from datetime import datetime, date
from collections import OrderedDict
from json import loads
from typing import Union, List, Any, Tuple, AnyStr, Dict

from requests import get, post
from sentry_sdk import capture_message

# LOG
# log.setLevel('DEBUG')


# FUNCTION
def update_token(client_id: str, client_secret: str) -> AnyStr:
    """Getting a workable token via OAuth service
    by passing id and secret obtained from API Console page.
    Every request to Cisco API MUST have valid and active access token

    Arguments:
        client_id {str} -- [a string of registered App @ API Console]
        client_secret {str} -- [a secret to be paired with id]

    Returns:
        [str] -- [an access token]
    """

    # preparing values
    params = {
        "grant_type": "client_credentials",
        "client_id": client_id,
        "client_secret": client_secret,
    }
    log.debug(f"~  AccessToken Updating")

    # requesting token via OAuth
    reply = post(UPDATE_TOKEN_URI, params=params)

    # raise an error if reply is not ok (HTTP_CODE: !200)
    if not reply.ok:
        raise ValueError(
            f"!!! AccessToken Error: {reply.status_code} {reply.text}")

    # or return an API token
    log.debug(f"+  AccessToken Updated")
    api_response: Dict[str, Any] = loads(reply.text)

    return api_response["access_token"]


def check_pi(pids_seq: str, token: str) -> Union[str, bool]:
    """A function to check via Cisco Product Information V1 API
    if provided PIDs (up to 5 per request) are valid

    Arguments:
        pids_seq {str} -- [sequence of PIDs comma separated]
        token {str} -- [token for request authorization]

    Returns:
        [list] -- [a list of data as API response]
    """

    # preparing a request
    url = (f"{PI_URI}{pids_seq}")
    auth = {"Authorization": f"Bearer {token}"}

    log.debug(f"~  PID Requesting: {len(pids_seq)} items")
    reply = get(url, headers=auth)

    # raise a value if response is not ok (HTTP_CODE: 200)
    if not reply.ok:
        log.warning(f"!!! PI Error: {reply.status_code} {reply.text}")
        return False

    # or return a part of a result
    log.debug(f"+ PID Received")
    api_response = loads(reply.text)
    data = api_response["product_list"]
    return data


def parse_api_response(api_response: Dict[str, str]):
    """To parse a response from Cisco API for EOX request
    
    Arguments:
        api_response [dict] -- [a dict with a response made from JSON]
    
    Returns:
        [item] -- [an OrderedDict of valuable information]
    """
    # grab only useful part of a dictionary
    data = api_response["EOXRecord"][0]

    # forming a dict of valuable information from a big API response
    item = OrderedDict()
    item["Name"] = data["EOXInputValue"].strip()
    item["Description"] = data["ProductIDDescription"]
    item["EoSale"] = data["EndOfSaleDate"]["value"]
    item["BulletinURL"] = data["LinkToProductBulletinURL"]
    item["BulletinDate"] = data["EOXExternalAnnouncementDate"]["value"]
    item["EoSupport"] = data["LastDateOfSupport"]["value"]
    item["EoSvcRenew"] = data["EndOfServiceContractRenewal"]["value"]

    migration = data["EOXMigrationDetails"]
    if migration["MigrationProductId"] != " ":
        item["ReplacementPID"] = migration["MigrationProductId"]
    else:
        item["ReplacementPID"] = migration["MigrationProductName"]
    item["PIDDescription"] = migration["MigrationInformation"]
    item["PIDInfoURL"] = migration["MigrationProductInfoURL"]

    return item


def check_eox(pid: str, token: str) -> Union[bool, Dict[str, Any]]:
    """A function to obtain via Cisco EOX V5 API
    a state of relevance for a provided PID (1 per request)

    Arguments:
        pid {str} -- [a product number]
        token {str} -- [token for request authorization]

    Returns:
        [OrderedDict] -- [a dictionary with API response]
    """

    # preparing a request
    url = (f"{EOX_URI}{pid}{RESPONSE_TYPE}")
    auth = {"Authorization": f"Bearer {token}"}

    # trying to request until response is ok, but not more than 3 times
    log.debug(f"~ EoX Requesting: {pid}")
    reply = get(url, headers=auth)

    # return False if response is not ok (HTTP_CODE: !200)
    if not reply.ok:
        log.warn(f"!!! EOX Error: {reply.status_code} {reply.text}")
        return False

    # or translate a JSON response to an OrderedDict
    api_response = loads(reply.text)

    item = parse_api_response(api_response)

    # announcing the relevance of PID
    if item["EoSale"]:
        log.info(f"+ Ended {pid}")
    else:
        log.info(f"+ Active {pid}")

    log.debug(f"... EoX Data: {item}")
    return item, api_response


def search_eox(raw_pids: str, token: str = None):
    """Stitching and making a queue of check_pi->check_eox

    Arguments:
        raw_pids {str} -- [pids from web-form]
        token {str} -- [Cisco API access token]

    Returns:
        [list, list, list, list] -- [eox_pids, obsolete_pids,
                                    active_pids, wrong_pids]
    """

    # updating an API access token
    if not token:
        log.debug(f"*** TokenUpdate Started")
        api_id, api_secret = api_client_creds()
        token = update_token(api_id, api_secret)

    # splitting a string of pids by space
    # normalizing capitalization
    # only uniq pids passed (a set to remove repeats)
    pids_list = list({pid.upper().strip() for pid in raw_pids.split()})

    ############# PI section #############
    pi_result: List[Any] = []

    # API supports not more than 5 pids within a single request
    # so we have to split a long list to shorter chunks
    log.info(f"*** PI Started")

    # chunking to sublists of 5 items if more than 5 pids passed
    if len(pids_list) > 5:
        chunks = [pids_list[x:x + 5] for x in range(0, len(pids_list), 5)]

        # then check for every chunk
        for chunk in chunks:
            pids_seq = ",".join(chunk)
            log.info(f"~ PI Working ...")
            reply = check_pi(pids_seq, token)
            # skip reply if reply is not ok
            if not reply:
                continue
            pi_result.append(reply)

    # or check all within a single PI request
    else:
        pids_seq = ",".join(pids_list)
        reply = check_pi(pids_seq, token)
        # add only if reply is ok
        if reply:
            pi_result.append(reply)

    # full responses to pass further
    api_responses = []

    # for items with End-of-X status
    eox_pids = []

    # for items with End-of-Life status
    obsolete_pids = []

    # for items that valid and active
    active_pids = []

    # for items that are invalid
    wrong_pids = []

    # excluding all invalid pids
    if pi_result:
        for part in pi_result:
            for item in part:
                pid = item["product_id"]
                error = item.get("ErrorResponse")
                if error:
                    wrong_pids.append(pid)
                    # reason = error["APIError"]["ErrorDescription"]
                    log.info(f"+ NotFound {pid}")
    # if there is no reply.ok then add all pids to wrong_pids
    else:
        for pid in pids_list:
            wrong_pids.append(pid)

    log.info(f"+++ PI Finished")
    log.debug(f"Wrong pids: {','.join(wrong_pids)}")

    ############# EoX section #############
    # gathering relevance information for 1 PID per request
    log.info(f"*** EoX Started")

    # one by one
    for pid in pids_list:
        # if pid is already presented in wrong_pids populated by check_pi
        # then there are no reasons to check a status
        if pid in wrong_pids:
            continue

        # asking Cisco API about EOX status of a pid
        item, api_response = check_eox(pid, token)

        # if not eoxed
        if item["Description"] is not "":
            # save to the list of all API responses
            api_responses.append(api_response)
            # check if obsolete
            fmt = r"%Y-%m-%d"
            eol_date = datetime.strptime(item["EoSupport"], fmt).__str__()
            today = date.today().__str__()
            # if end-of-life date reached then add to obsolete
            if eol_date < today:
                obsolete_pids.append(item)
            # else to eox
            else:
                eox_pids.append(item)
        # else it's an active pid
        else:
            active_pids.append(item['Name'])

    log.debug(f"EoX pids: {eox_pids}")
    log.debug(f"Obsolete pids: {obsolete_pids}")
    log.debug(f"Active pids: {','.join(active_pids)}")
    log.info(f"+++ EoX Finished")
    capture_message(f'Processed a request: {raw_pids}')

    # return all we have
    return eox_pids, obsolete_pids, active_pids, wrong_pids, api_responses
