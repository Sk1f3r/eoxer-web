# IMPORT
from logging import basicConfig, getLogger
from os import environ, path, chdir
from typing import Tuple

from decouple import config, AutoConfig

# LOG
FORMAT = '[%(asctime)s] | %(message)s'
basicConfig(format=FORMAT, level="INFO")
log = getLogger(__name__)

# URI parts
PI_URI = "https://api.cisco.com/product/v1/information/product_ids/"
EOX_URI = "https://api.cisco.com/supporttools/eox/rest/5/EOXByProductID/1/"
UPDATE_TOKEN_URI = "https://cloudsso.cisco.com/as/token.oauth2"

RESPONSE_TYPE = "?responseencoding=json"

# DB
MONGODB_DB_NAME = "ccc"
MONGODB_COLLECTION = "data"

# VAR
PROJ_DIR = path.abspath(path.dirname("."))
DATA_DIR = path.join(PROJ_DIR, "data")
DATA_SHEET_NAME = "Sku Item Management Report"


# FUNCTIONS
def get_sentry_dsn(path: str = ".env") -> str:
    " Load SENTRY_TOKEN from .env file and make SENTRY_DSN "

    sentry_token = config('SENTRY_TOKEN', default=False)
    if sentry_token:
        sentry_dsn = f"https://{sentry_token}@sentry.io/1282506"
        return sentry_dsn
    return None


def api_client_creds(dev: bool = False, path: str = ".") -> Tuple[str, str]:
    """[Setting client credentials that used to request access token]

    If any debug flags are set then firstly try to load creds with DEV_ prefix,
    else load PROD_ prefix.
    
    Keyword Arguments:
        dev {bool} -- [DEV prefixed creds loaded if True] (default: {False})
    
    Raises:
        Exception -- [if creds cannot be loaded then Exception]
    
    Returns:
        Tuple[str, str] -- [api_id and api_secret string returned]
    """

    # set path to read another .env if required, e.g. for tests
    config = AutoConfig(search_path=path)

    # preparing vars
    api_creds_loaded = False

    # is debug mode enabled somehow?
    dev_mode = environ.get('DEV_MODE') == "1"
    flask_debug = environ.get('FLASK_DEBUG') == "1"

    # load DEV_ prefixed envs if so
    if dev or dev_mode or flask_debug:
        api_id = config('DEV_API_ID', default=False)
        api_secret = config('DEV_API_SECRET', default=False)
        if api_id and api_secret:
            log.info(f"DEV env loaded")
            api_creds_loaded = True

    # else load PROD_ prefixed envs
    if not api_creds_loaded:
        api_id = config('PROD_API_ID', default=False)
        api_secret = config('PROD_API_SECRET', default=False)
        if api_id and api_secret:
            log.info(f"PROD env loaded")
            api_creds_loaded = True
        else:
            raise Exception('Env cannot be loaded')

    return api_id, api_secret
