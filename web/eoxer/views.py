# IMPORT
from os import path, remove
from time import strftime, localtime
from collections import OrderedDict
from json import loads, dumps

from flask import render_template, request, current_app
from flask import Response, Blueprint
from flask_excel import make_response_from_records
from werkzeug.utils import secure_filename

from eoxer.search_eox import search_eox, parse_api_response
from eoxer.search_cc import search_cc_version, search_cc_substring
from eoxer.update_cc import update_cc
from eoxer.forms import PidForm, CcFileForm

# BLUEPRINT
bp_index = Blueprint('_index', "/")
bp_search_eox = Blueprint('_search_eox', __name__)
bp_export_eox = Blueprint('_export_eox', __name__)
bp_search_cc = Blueprint('_search_cc', __name__)
bp_update_cc = Blueprint('_update_cc', __name__)
bp_help = Blueprint('_help', __name__)

all_blueprints = (bp_index, bp_search_eox, bp_export_eox, bp_search_cc,
                  bp_update_cc, bp_help)


# ROUTE
# Start page
@bp_index.route('/', methods=['GET'])
def _index():
    form = PidForm()

    # just a normal page
    return render_template('index.html', form=form)


# Result page
@bp_search_eox.route('/search_eox', methods=['GET'])
def _search_eox():
    # collecting passed args and passing them to a form
    args = request.args
    form = PidForm(args)

    # if validation went successfully
    if form.validate():

        # extracting a list of pids
        raw_pids = request.args.get('pids')

        # do the job
        raw_eox, raw_obsolete, raw_active, raw_wrong, api_responses = search_eox(
            raw_pids)

        # prepare the lists of active and wrong pids
        active_pids = ", ".join(raw_active).strip(", ")
        wrong_pids = ", ".join(raw_wrong).strip(", ")

        # return a result
        return render_template(
            'search_eox.html',
            eox_pids=raw_eox,
            obsolete_pids=raw_obsolete,
            active_pids=active_pids,
            wrong_pids=wrong_pids,
            api_responses=api_responses)

    return render_template("index.html", form=form), 307


# Export EOX result to an Excel file
@bp_export_eox.route('/export_eox', methods=['POST'])
def _export_eox():
    # extracting json with EoXed PIDs from vars
    api_responses = request.values.get("api_responses")

    # extracting Active pids from vars
    active_pids = request.values.get("active_pids")

    # reformat in the same way as it happens in the function check_eox()
    items = []
    # value a string as Dict()
    e_api_responses = eval(api_responses)
    for api_response in e_api_responses:
        item = parse_api_response(api_response)

        # append OrderedDict of item to a list of items
        items.append(item)

    # add Active pids to the list "items"
    for pid in active_pids.split(", "):
        item = OrderedDict()
        item["Name"] = pid
        item["Description"] = "Актуально"
        items.append(item)

    # prepare the name of a file
    now = strftime("%Y-%m-%d_%H-%M", localtime())
    filename = f"eox-result_{now}"

    # return as an Excel file
    return make_response_from_records(items, "xls", file_name=filename)


# Result page
@bp_search_cc.route('/search_cc', methods=['GET'])
def _search_cc():
    # collecting passed args and passing them to a form
    args = request.args
    form = PidForm(args)

    # if validation went successfully
    if form.validate():

        # extracting a list of pids
        raw_pids = request.args.get('pids')

        # obtaining configuration to set up a connection to a database
        db = current_app.config['MONGODB_URI']

        # do the job
        version = search_cc_version(db)
        c1, c2, c3, cX, not_found = search_cc_substring(raw_pids, db)

        # prepare the list of not_found
        nf = ", ".join(not_found).strip(", ")

        # return a result
        return render_template(
            'search_cc.html',
            version=version,
            c1=c1,
            c2=c2,
            c3=c3,
            cX=cX,
            nf=nf)

    return render_template("index.html", form=form), 307


# Update db with new xls data
@bp_update_cc.route('/update_cc', methods=['GET', 'POST'])
def _update_cc():
    # entering manually to the page
    if request.method == 'GET':
        # obtaining configuration to set up a connection to a database
        db = current_app.config['MONGODB_URI']
        # grabbing the version
        version = search_cc_version(db)
        # preparing the form
        form = CcFileForm()
        # return just a normal page
        return render_template(
            "update_cc.html", method='get', version=version, form=form)

    # POST method by form action
    elif request.method == 'POST':
        # applying arguments from form to wtform
        form = CcFileForm(request.files)
        # if validation went successfully
        if form.validate():
            file = request.files['ccfile']
            # file = request.files['ccfile'].filename
            name = file.filename
            # making a full path
            full = path.join(".", name)
            filename = secure_filename(full)

            # saving a file locally
            file.save(filename)

            # do the job with the file of this name
            try:
                # obtaining configuration to set up a connection to a database
                db = current_app.config['MONGODB_URI']
                # do the job
                result = update_cc(filename, db)
            except KeyError:
                # if list not found
                return Response("Не найден лист Cisco Product List Russia")
            else:
                # if worked out then show the result
                return render_template(
                    "update_cc.html", method='post', result=result)
            finally:
                # delete the xls file afterall
                remove(filename)

        return render_template("update_cc.html", method='get', form=form), 307


@bp_help.route("/help")
def help():
    return render_template("help.html")
