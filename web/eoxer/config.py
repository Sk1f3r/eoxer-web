# IMPORT


# CLASS
class Config(object):
    ADMINS = ['sk1f3r@gmail.com']


class ProdConfig(Config):
    """ Production config """
    ENV = 'prod'
    DEBUG = False
    WTF_CSRF_ENABLED = False
    SECRET_KEY = "PROD: aint that secret haha"
    MONGODB_HOST = "db"
    MONGODB_URI = f"mongodb://{MONGODB_HOST}:27017/"


class DevConfig(Config):
    """ Development config """
    ENV = 'dev'
    DEBUG = True
    WTF_CSRF_ENABLED = False
    SECRET_KEY = "DEV: aint that secret hehe"
    MONGODB_HOST = "db"
    MONGODB_URI = f"mongodb://{MONGODB_HOST}:27017/"


class TestConfig(Config):
    """ Testing config """
    ENV = 'test'
    TESTING = True
    DEBUG = True
    WTF_CSRF_ENABLED = False
    SECRET_KEY = "TEST: aint that secret hoho"
    MONGODB_HOST = "127.0.0.1"
    MONGODB_URI = f"mongodb://{MONGODB_HOST}:27017/"
