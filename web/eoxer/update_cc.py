# IMPORT
from eoxer.database import DBHelper
from eoxer.vars import DATA_SHEET_NAME

from re import search
from os import path

from pyexcel import get_book


# DEF
def xls_to_list(book: str, sheet: str = DATA_SHEET_NAME):
    "Translate content of an Excel file to local list"
    _book = get_book(file_name=book)
    _sheet = _book[sheet]
    storage = []
    for i in _sheet:
        dct = dict()

        pid = i[0].strip()
        cat_r = i[5].strip()
        cat_c = i[6].strip()
        dct["pid"] = pid
        dct["cat_r"] = cat_r
        dct["cat_c"] = cat_c

        storage.append(dct)

    return storage


def list_to_collection(dct, collection):
    "Write the local dictionary to MongoDB"
    try:
        collection.remove()
    except Exception as e:
        print(e.__doc__())
    else:
        for i in dct:
            collection.insert(i, check_keys=False)


def update_cc(xlsfile: str, db: str):
    "Stiching all functions to make it happen"

    # preparing records
    _list = xls_to_list(xlsfile)

    # client to work with MongoDB
    client = DBHelper(host=db)

    # getting current version
    _version = client.find_version()
    # if such one exists
    if _version:
        old_version = _version['version']
    else:
        old_version = None

    # clearing records and write new
    result = client.collection_clear()
    if not result:
        return None
    client.collection_update(_list)

    # extracting new version and write it into DB
    filename = xlsfile.split(path.sep)[-1]
    new_version = search(r"^SkuManagement_Report_(\S+)\.xlsx?$", filename).group(1)
    client.version_update(new_version)

    # delete header row
    client.junk_delete()

    # collecting statistics
    count = client.count()

    if not count:
        return None

    result = {
        "count": count,
        "old_version": old_version,
        "new_version": new_version
    }

    return result
