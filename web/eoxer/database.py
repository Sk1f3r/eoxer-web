# IMPORT
from pymongo import MongoClient
from eoxer.vars import MONGODB_DB_NAME, MONGODB_COLLECTION

# VAR
nil_id = {"_id": 0}


# CLASS
class DBHelper:
    def __init__(self,
                 host="localhost",
                 db_name=MONGODB_DB_NAME,
                 col_name=MONGODB_COLLECTION):
        self.host = host
        self.db_name = db_name
        self.col_name = col_name
        client = MongoClient(host)
        db = client[db_name]
        col = db[col_name]
        self.client = client
        self.col = col

    def find_version(self):
        return self.col.find_one({"version": {"$exists": "true"}}, nil_id)

    def find_substring(self, substring):
        self.substring = substring.upper()
        return self.col.find({"pid": {"$regex": self.substring}}, nil_id)

    def collection_clear(self):
        result = self.col.delete_many({})
        if not result:
            return "Ошибка: не удалось очистить базу данных"
        return result

    def collection_update(self, lst):
        self.lst = lst
        # for record in self.lst:
        result = self.col.insert_many(
            self.lst, ordered=False)
        if not result:
            return "Ошибка: не удалось обновить базу данных"
        return result

    def version_update(self, version):
        self.version = version
        result = self.col.insert_one({"version": self.version})
        if not result:
            return "Ошибка: не удалось обновить запись версии"
        return True

    def junk_delete(self):
        result = self.col.delete_one({"pid": "ProductID"})
        if not result:
            return "Ошибка: не удалось удалить служебные записи"
        return True

    def count(self):
        result = self.col.find({"pid": {"$exists": "true"}}, nil_id).count()
        if not result:
            return "Ошибка: не удалось подсчитать количество записей"
        return result

    def __del__(self):
        self.client.close()
