# IMPORT
from os import path

from flask import Flask
from jinja2_markdown import MarkdownExtension
from sentry_sdk import init as sentry_init
from flask_excel import init_excel

from eoxer.config import ProdConfig
from eoxer.views import all_blueprints
from eoxer.errors import eh
from eoxer.vars import get_sentry_dsn

# CONST
APP_DIR = path.abspath(path.dirname(__file__))
TEMPLATE_DIR = path.join(APP_DIR, 'templates')
STATIC_DIR = path.join(APP_DIR, 'static')


# FUNCTION
def create_app(config_object=ProdConfig):
    """ An application factory """
    # creating an instance of app
    app = Flask(
        __name__,
        instance_relative_config=True,
        template_folder=TEMPLATE_DIR,
        static_folder=STATIC_DIR)
    
    # excel module
    init_excel(app)

    # instance of Sentry
    sentry_dsn = get_sentry_dsn()
    if sentry_dsn:
        sentry_init(sentry_dsn)

    # does not strictly require slashes
    app.url_map.strict_slashes = False

    # load up a config
    app.config.from_object(config_object)

    # registering blueprints
    for bp in all_blueprints:
        app.register_blueprint(bp)

    # registering errorhandlers
    app.register_blueprint(eh)

    # adding extentions
    # support of markdown to jinja
    app.jinja_env.add_extension(MarkdownExtension)

    return app
