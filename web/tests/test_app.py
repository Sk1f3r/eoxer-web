''' Pytest file to test Flask application '''
# IMPORT
import pytest


# TEST
@pytest.mark.app
def test_index_page_get(test_yield_app):
    """
    GIVEN a Flask app
    WHEN got response for GET request to '/' page
    THEN check that HTTP status code is 200
    """
    app = test_yield_app
    response = app.get('/')
    assert response.status_code == 200


@pytest.mark.app
def test_check_short_pid(test_yield_app):
    """
    GIVEN a Flask app
    WHEN got response for GET request to '/search_eox' page
    THEN check that HTTP status code is not 200 as pid is too short
    """
    app = test_yield_app
    response = app.get('/search_eox', query_string=dict(pids="qwe"))
    assert response.status_code != 200


@pytest.mark.app
def test_check_comma_pids(test_yield_app):
    """
    GIVEN a Flask app
    WHEN got response for GET request to '/search_eox' page
    THEN check that HTTP status code is not 200 as pids contain a comma char
    """
    app = test_yield_app
    response = app.get('/search_eox', query_string=dict(pids="first, second"))
    assert response.status_code != 200


@pytest.mark.app
def test_index_page_post(test_yield_app):
    """
    GIVEN a Flask app
    WHEN got response for POST request to '/' page
    THEN check that HTTP status code is any, but 200
    """
    app = test_yield_app
    response = app.post('/')
    assert response.status_code != 200


@pytest.mark.app
def test_check_page_post(test_yield_app):
    """
    GIVEN a Flask app
    WHEN got response for POST request to '/search_eox' page
    THEN check that HTTP status code is any, but 200
    """
    app = test_yield_app
    response = app.post('/search_eox')
    assert response.status_code != 200


@pytest.mark.app
def test_check_page_get_pid(test_yield_app):
    """
    GIVEN a Flask app
    WHEN got response for GET request with proper data to '/search_eox' page
    THEN check that HTTP status code is 200
    """
    app = test_yield_app
    response = app.get('/search_eox', query_string=dict(pids="test-pid"))
    assert response.status_code == 200


@pytest.mark.app
def test_check_page_get_without_pids(test_yield_app):
    """
    GIVEN a Flask app
    WHEN got response for GET request without any data to '/search_eox' page
    THEN check that HTTP status code is any, but 200
    """
    app = test_yield_app
    response = app.get('/search_eox')
    assert response.status_code != 200
