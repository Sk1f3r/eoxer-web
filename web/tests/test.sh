#!/usr/bin/env bash

log() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
}
python3 -m venv .venv &&
source .venv/bin/activate

if ! python3 -m pip install --no-cache-dir --disable-pip-version-check poetry; then
  log "Unable to install poetry"
  exit 1
else
  log "poetry installed, ready to prepare venv"
  poetry install && \
  pytest
fi
