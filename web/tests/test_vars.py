# IMPORT
import pytest
from eoxer.vars import api_client_creds
from os import environ

# VAR
ENVS = ['DEV_API_ID', 'PROD_API_ID', 'DEV_MODE', 'FLASK_DEBUG']


# TEST
@pytest.mark.env
def test_load_dev_var_env(test_yield_env_file):
    """
    GIVEN IF LOCAL TEST prepared and filled dotenv file
    GIVEN IF CI/CD TEST prepared testing environment
    WHEN dev flag passed to api_client_creds function
    THEN check that creds with DEV_ prefix are loaded
    """
    # ensure system env is pure
    for e in ENVS:
        if environ.get(e):
            environ.pop(e)

    # retrieving path to test .env
    env_file, env_dir = test_yield_env_file

    # extracting creds - dev var is True
    api_id, api_secret = api_client_creds(dev=True, path=env_dir)

    # asserting
    assert api_id
    assert api_secret
    assert type(api_id) == str
    assert type(api_secret) == str


@pytest.mark.env
def test_load_dev_mode_env(test_yield_env_file, monkeypatch):
    """
    GIVEN IF LOCAL TEST prepared and filled dotenv file
    GIVEN IF CI/CD TEST prepared testing environment
    WHEN system environment has 'DEV_MODE' equals to "1"
    THEN check that creds with DEV_ prefix are loaded
    """
    # ensure system env is pure
    for e in ENVS:
        if environ.get(e):
            environ.pop(e)

    # retrieving path to test .env
    env_file, env_dir = test_yield_env_file

    # preparing system env
    monkeypatch.setenv('DEV_MODE', '1')

    # extracting creds
    api_id, api_secret = api_client_creds(path=env_dir)

    # asserting
    assert api_id
    assert api_secret
    assert type(api_id) == str
    assert type(api_secret) == str


@pytest.mark.env
def test_load_prod_env(test_yield_env_file):
    """
    GIVEN prepared and filled dotenv file
    WHEN no dev flag passed to api_client_creds function
    WHEN no 'DEV_MODE' set to "1"
    THEN check that creds with PROD_ prefix are loaded
    """
    # ensure system env is pure
    for e in ENVS:
        if environ.get(e):
            environ.pop(e)

    # retrieving path to test .env
    env_file, env_dir = test_yield_env_file

    # extracting creds - no dev, no DEV_MODE
    api_id, api_secret = api_client_creds(path=env_dir)

    # asserting
    assert api_id
    assert api_secret
    assert type(api_id) == str
    assert type(api_secret) == str


@pytest.mark.temp
@pytest.mark.env
def test_load_dev_mode_osenv(test_yield_env_file_empty, monkeypatch):
    """
    GIVEN system environment of enabled debug with monkeypatched envvars
    WHEN system environment has 'DEV_MODE' equals to "1"
    THEN check that creds with DEV_ prefix are loaded
    """
    # ensure system env is pure
    for e in ENVS:
        if environ.get(e):
            environ.pop(e)

    # retrieving path to test envs
    env_file, env_dir = test_yield_env_file_empty

    # preparing system env
    monkeypatch.setenv('DEV_MODE', '1')
    monkeypatch.setenv('DEV_API_ID', 'empty_dev_api_id')
    monkeypatch.setenv('DEV_API_SECRET', 'empty_dev_api_secret')

    # extracting creds
    api_id, api_secret = api_client_creds(path=env_dir)

    # asserting
    assert api_id == "empty_dev_api_id"
    assert api_secret == "empty_dev_api_secret"
