''' Pytest file to test Cisco API where requests reply is ok (200) '''
# IMPORT
from eoxer.search_eox import check_pi, check_eox, search_eox

from collections import OrderedDict
import pytest

# VAR
TEST_PID_VALID = "ISR4331/K9"
TEST_PID_INVALID = "Greymane"
TEST_PIDS_SIX_VALID = "PVDM2-8 PVDM2-16 PVDM2-32 PVDM2-48 PVDM2-64 GLC-T"
TEST_PIDS = "ISR4451-X/K9 CISCO2801 WAVE-694-K9 GE-T="


# TEST
@pytest.mark.api
def test_is_pi_works_valid_pid(test_yield_api_token):
    """
    GIVEN a Cisco API access token
    GIVEN a valid PID
    WHEN got response from Product Information V1 API
    THEN check that response is a list
    THEN check that PID is valid
    """
    pid = TEST_PID_VALID
    token = test_yield_api_token
    response = check_pi(pid, token)
    result = response[0]['product_type']
    expected = 'ROUTER'

    assert isinstance(response, list)
    assert result == expected


@pytest.mark.api
def test_is_pi_works_invalid_pid(test_yield_api_token):
    """
    GIVEN a Cisco API access token
    GIVEN an invalid PID
    WHEN got response from Product Information V1 API
    THEN check that response is a list
    THEN check that PID is invalid
    """
    pid = TEST_PID_INVALID
    token = test_yield_api_token
    response = check_pi(pid, token)
    result = response[0]['ErrorResponse']['APIError']['ErrorCode']
    expected = 'NO_RECORDS_FOUND'

    assert isinstance(response, list)
    assert result == expected


@pytest.mark.api
def test_is_eox_works(test_yield_api_token):
    """
    GIVEN a Cisco API access token
    GIVEN a valid PID
    WHEN got response from EOX V5 API
    THEN check that response is an OrderedDict
    THEN check that Name equals to PID itself
    """
    pid = TEST_PID_VALID
    token = test_yield_api_token
    response, _ = check_eox(pid, token)
    result = response['Name']
    expected = TEST_PID_VALID

    assert isinstance(response, OrderedDict)
    assert result == expected


@pytest.mark.skip
@pytest.mark.api
def test_is_main_works_with_six_valid_pids(test_yield_api_token):
    """
    GIVEN a Cisco API access token
    GIVEN a list of 6 valid PIDs
    WHEN got response from Product Information V1 API
    THEN check that response is a list
    THEN check that PID is valid
    """
    pid = TEST_PIDS_SIX_VALID
    token = test_yield_api_token
    response = search_eox(pid, token)
    assert len(response[0]) == 6


@pytest.mark.api
def test_is_full_works(test_yield_api_token):
    """
    GIVEN a Cisco API access token
    GIVEN a set of various PIDs
    WHEN got result from main check function
    THEN check that 1st item from pids is active
    THEN check that 2nd item from pids is obsolete
    THEN check that 3rd item from pids is eox
    THEN check that 4th item from pids is wrong
    """
    pids = "ISR4451-X/K9 CISCO2801 WAVE-694-K9 GE-T="
    api_token = test_yield_api_token
<<<<<<< HEAD
    eox, obsolete, active, wrong, _ = search_eox(pids, token=api_token)
=======
    eox, obsolete, active, wrong = search_eox(pids, token=api_token)
>>>>>>> + cc feature, + help page (jinja2_md), + eh (404/500)
    expected_active = "ISR4451-X/K9"
    expected_obsolete = "CISCO2801"
    expected_eox = "WAVE-694-K9"
    expected_wrong = "GE-T="
    assert eox[0]['Name'] == expected_eox
    assert obsolete[0]['Name'] == expected_obsolete
    assert active[0] == expected_active
    assert wrong[0] == expected_wrong
