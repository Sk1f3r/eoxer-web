# IMPORT
import pytest
from requests import get

# CONST
GL_REGISTRY_URL = "registry.gitlab.com"


# FUNCTION
def setup_module(module):
    pass


def teardown_module(module):
    pass


@pytest.mark.gitlab
@pytest.mark.trylast
def test_registry_http():
    ''' Test of Gitlab registry is available '''
    response = get(f"http://{GL_REGISTRY_URL}")
    result = response.status_code
    expected = 200
    assert result == expected
