''' Pytest file to test Cisco API where requests reply is not ok (!200) '''
# IMPORT
from eoxer.search_eox import update_token, search_eox

import pytest


# TEST
@pytest.mark.api
def test_wrong_api_creds():
    """
    GIVEN wrong Cisco API id and secret
    WHEN trying to obtain an access_token with such creds
    THEN check that the function raises ValueError
    """
    wrong_api_id = 'hanzo'
    wrong_api_secret = 'genji'
    with pytest.raises(ValueError):
        update_token(wrong_api_id, wrong_api_secret)


@pytest.mark.api
def test_check_pi_reply_is_not_ok(test_yield_api_token):
    """
    GIVEN a Cisco API access token
    GIVEN an incorrect_pid
    WHEN got response from Product Information V1 API
    THEN check that the function returns False as reply isn't ok
    """
    api_token = test_yield_api_token
    incorrect_pid = '/////'
    result = search_eox(incorrect_pid, api_token)
    assert result[3] == [incorrect_pid]
