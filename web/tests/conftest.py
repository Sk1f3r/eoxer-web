# IMPORT
from sys import platform
from os import environ

import pytest


# FIXTURE
@pytest.fixture()
def test_yield_env_file(tmpdir):
    '''
    Fixture to yield filled .env file
    to test api_client_creds function from eoxer.vars
    '''
    ENV_DATA = '''DEV_API_ID="dev_api_id"
DEV_API_SECRET="dev_api_secret"
PROD_API_ID="prod_api_id"
PROD_API_SECRET="prod_api_secret"'''
    # env = tmpdir.mkdir("test").join(".env")
    env_dir = tmpdir.mkdir("test")
    env_file = env_dir.join(".env")
    env_file.write(ENV_DATA)
    yield env_file, env_dir


@pytest.fixture()
def test_yield_env_file_empty(tmpdir):
    '''
    Fixture to yield empty .env file
    to test api_client_creds function from eoxer.vars
    '''
    ENV_DATA = "NONE=0"
    env_dir = tmpdir.mkdir("test")
    env_file = env_dir.join(".env")
    env_file.write(ENV_DATA)
    yield env_file, env_dir


@pytest.fixture(scope="session")
def test_yield_app():
    '''
    Fixture to yield Flask application instance
    to test views
    '''
    # prepare env
    environ['DEV_MODE'] = "1"

    # import
    from eoxer.app import create_app
    from eoxer.config import DevConfig

    # new app with DevConfig
    app = create_app(config_object=DevConfig)

    # Werkzeug test client
    test_client = app.test_client(use_cookies=True)

    # adding app context
    ctx = app.app_context()
    ctx.push()

    # yielding the object
    yield test_client

    # cleaning up
    ctx.pop()


@pytest.fixture(scope="session")
def test_yield_api_token():
    '''
    Fixture to yield Cisco API Access token
    to test API
    '''
    # import
    from eoxer.vars import api_client_creds
    from eoxer.search_eox import update_token

    # prepare env
    environ['DEV_MODE'] = "1"

    # setting vars
    api_id, api_secret = api_client_creds()

    # receiving an access token
    api_token = update_token(api_id, api_secret)

    # yielding the object
    yield api_token

    # cleaning up
    if environ.get('DEV_MODE'):
        environ.pop('DEV_MODE')


# MARK
skip_windows = pytest.mark.skipif(
    platform == 'win32', reason="will fail on windows")
fail_on_win = pytest.mark.xfail(
    platform == 'win32', reason="should fail on windows")
skip_linux = pytest.mark.skipif(
    platform == 'linux', reason="will fail on linux")
fail_on_linux = pytest.mark.xfail(
    platform == 'linux', reason="should fail on linux")
