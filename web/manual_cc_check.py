#!/usr/bin/env python3
''' Manual test of pids '''

# IMPORT
from eoxer.search_cc import search_cc_version, search_cc_substring

# CONST
MONGODB_HOST = "192.168.50.10"
MONGODB_URI = f"mongodb://{MONGODB_HOST}:27017/"
PID = "ASA5508-K8123"

# VAR
db = MONGODB_URI


# FUNCTION
def main(pid=PID.upper()):
    version = search_cc_version(db)
    pid_result = search_cc_substring(pid, db)
    return version, pid_result


# MAIN
if __name__ == "__main__":
    version, pid_result = main()
    if version and pid_result:
        dlmtr = "=" * 10
        print(f'{dlmtr} Version {dlmtr}\n{version}\n')
        print(f'{dlmtr} PID Result {dlmtr}\n{pid_result}')
