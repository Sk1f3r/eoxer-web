#!/usr/bin/env python3
''' Manual test of pids '''

# IMPORT
from eoxer.search_eox import check_eox, check_pi, update_token
from eoxer.vars import api_client_creds

# CONST
PID = "ASA5508-K8"

# VAR
api_id, api_secret = api_client_creds(dev=True)
api_token = update_token(api_id, api_secret)


# FUNCTION
def main(pid=PID.upper()):
    pi = check_pi(pid, api_token)
    # pi = True
    if pi:
        eox = check_eox(pid, api_token)
        return pi, eox


# MAIN
if __name__ == "__main__":
    pi, eox = main()
    if pi and eox:
        dlmtr = "=" * 10
        print(f'{dlmtr} Product Information {dlmtr}\n{pi}\n')
        print(f'{dlmtr} EOX {dlmtr}\n{eox}')
