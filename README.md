# EOXer

The way to obtain status information of Cisco partnumbers via splendidly simple web GUI.

## Running the app

### Stage 1. Cisco API

To workwith Cisco API there are few actions to be done:

1.0 to have active SNTC or PSS contract

2.1 request a role "API Console User" from your Designated Company Administrator

OR

2.2 write an email to tac@cisco.com and "Request access to PSS APIs and the API Console". Active SNTC or PSS contract has to be provide during conversation

2.3 register an Application with Grant Type "client credentials" at [**API Console portal**](https://apiconsole.cisco.com/apps/myapps) with two following APIs: **`"Product Info API 1.0"`** and  **`"EOX V5 API"`**

2.4 obtain **`"Client ID"`** and **`"Client Secret"`** from information a page with information for previously registered Application

### Stage 2. Starting containers

The whole app works as a pair of two containers and to start it all you need is following simple steps:

1. Clone the repository to your host: `git clone https://gitlab.com/Sk1f3r/eoxer-web.git`
1. Add two strings to file **`.env`** at root level of cloned repo: **`API_ID=your-client-id-from-cisco-api-console`** and **`API_SECRET=your-client-secret-from-cisco-api-console`**
1. run things up: **`docker-compose -f docker-compose.production.yml -d up`**

Ensure that both nginx and eoxer containers are up and running and if so go to [http://your-host-ip](http://your-host-ip)
